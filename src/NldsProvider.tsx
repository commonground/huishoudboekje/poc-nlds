import React, {useEffect} from "react";

const NldsProvider: React.FC<{children: any}> = ({children}) => {
	useEffect(() => {
		require("@utrecht/design-tokens/dist/index.css");

		// Add utrecht-theme class to the html.
		const html = document.querySelector("html");
		html?.classList.add("utrecht-theme");

		// Append web-component-library script to the end of the body.
		const script = document.createElement("script");
		script.src = "https://unpkg.com/@utrecht/web-component-library-stencil@latest/dist/utrecht/utrecht.esm.js";
		script.type = "module";

		document.querySelector("body")?.append(script);
	}, []);

	return (
		<>{children}</>
	);
};

export default NldsProvider;