import {UtrechtContactCardTemplate, UtrechtDigidLogo, UtrechtDocument, UtrechtHeading, UtrechtParagraph} from "@utrecht/web-component-library-react";
import React from "react";

/*
 * Naar voorbeeld van https://nl-design-system.github.io/utrecht/?path=/docs/react-component-readme--page
 * Ik heb niet de indruk dat er stijlelementen van Utrecht zichtbaar worden.
 */
const App = () => {
	return (
		<UtrechtDocument>
			<UtrechtHeading level={2}>Home</UtrechtHeading>
			<UtrechtParagraph>Hello, world!</UtrechtParagraph>

			<UtrechtContactCardTemplate />

			<UtrechtDigidLogo>Whatever</UtrechtDigidLogo>
		</UtrechtDocument>
	);
};

export default App;