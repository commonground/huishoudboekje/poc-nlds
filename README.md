# Proof of Concept NL Design System

Dit is een poging tot een minimale implementatie van het [NL Design System voor Utrecht](https://github.com/nl-design-system/utrecht).

Dit betreft een React-app.

Op basis van de branch `develop` is [hier een voorbeeld](https://poc-nlds.onrender.com/) gedeployed.